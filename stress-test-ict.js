import { sleep } from 'k6';
import http from 'k6/http';

export const options = {
  duration: '10s',
  vus: 50,
  thresholds: {
    http_req_duration: ['p(40)<1000'], // 95 percent of response times must be below 500ms
  },
};

export default function () {
  http.get('https://mydigital.cloud-connect.asia/');
  sleep(3);
}
